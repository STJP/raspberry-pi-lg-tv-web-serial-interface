
# LG TV 50PS8 RS232 remote control 
# Adapted by Stephen Peek
# 
# Ver 1.0 

import serial
import re
import time

# TV requires Baudrate:9600, Data Length:8 bit, Parity:None, Stop Bit:1 Bit   

class LG():
	def __init__(self, comport, baudrate = 9600, address = 1): # %d used to pass to commands
		self.comport = comport
		self.baudrate = baudrate
		self.address = address
		self._inst = serial.Serial(self.comport, baudrate=self.baudrate)

#	Write set used to send commands to the TV   
	def write_set(self, message):
		print ("set",message)
		self._inst.write(message)

#	Write get not used in this code    
	def write_get(self, message):
		self._inst.write(message)
		ret = self._inst.read(9)
		print(ret)
#  should parse ret
#  pick last field, strip off 'OK' and '\n'
#  fields = re.split(' +', ret)
#  return int(fields[-1][2:],16)
		return(ret)

# Responce from TV not avaliable when TV is Power is Off   
#    def get_power(self):
#		self._inst.write("ka %d ff\r" % (self.address))
#		ret = self._inst.read(9)
#		print("gp", ret)
#		return(ret)
	
	def set_power(self, v):
		if v == 0 or v == "off":
			self.write_set("ka %d 0\r" % (self.address))
		if v == 1 or v == "on":
			self.write_set("ka %d 1\r" % (self.address))
   
	def set_input(self, input):
		input = input.lower()
		print (input)
		input_dict = {
					'dtv':'00',
					'atv':'01',
					'av1':'20',
					'av2':'21',
					'component1':'40',
					'component2':'41',
					'hdmi1':'90',
					'hdmi2':'91',
					'hdmi3':'92',
					'hdmi4':'93',
					'vga':'60'
					}
                      
		self.write_set("xb %d %s\r" % (self.address, input_dict[input]))
   
    
	def set_volume(self, level):
        #convert level to hex
		level = hex(level)[2:]
		self.write_set("kf %d %s\r" % (self.address, level))

#	TV Remote Power On/Off
	def set_remotepower(self):
		self.write_set("mc %d 08\r" % (self.address))
        
#	TV Mute On/Off
	def set_mute(self):
		self.write_set("mc %d 09\r" % (self.address))

#	TV Guide On/Off
	def set_guide(self):
		self.write_set("mc %d AB\r" % (self.address))

	def set_channel(self, channel):
		chan = channel
		self.write_set("mc %d %s\r" % (self.address, chan))		

#	Info On/Off
	def set_info(self):
		self.write_set("mc %d AA\r" % (self.address))

#	TV Up Arrow		
	def set_uparrow(self):
		self.write_set("mc %d 40\r" % (self.address))

#	TV Down Arrow
	def set_dnarrow(self):
		self.write_set("mc %d 41\r" % (self.address))

#	TV Right Arrow
	def set_rightarrow(self):
		self.write_set("mc %d 06\r" % (self.address))
		
#	TV Left Arrow
	def set_leftarrow(self):
		self.write_set("mc %d 07\r" % (self.address))

#	TV OK Button
	def set_ok(self):
		self.write_set("mc %d 44\r" % (self.address))

#	TV Volume Up
	def set_volup(self):
		self.write_set("mc %d 02\r" % (self.address))

#	TV Volume Down
	def set_voldn(self):
		self.write_set("mc %d 03\r" % (self.address))

#	TV Program Up 
	def set_progup(self):
		self.write_set("mc %d 00\r" % (self.address))

#	TV Program Down		
	def set_progdn(self):
		self.write_set("mc %d 01\r" % (self.address))
		
#	TV Return/Exit Button
	def set_returnexit(self):
		self.write_set("mc %d 28\r" % (self.address))
		
