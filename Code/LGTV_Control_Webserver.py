#!/user/bin/env python

# LG TV 50PS8 RS232 remote control 
# Adapted by Stephen Peek
# 
# Ver 1.0 

import string,cgi,time
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer


import LGTV_Commands

class MyHandler(BaseHTTPRequestHandler):

	def do_POST(self):		
        
		try:

			self.send_response(301)

#	Separated the commands, any command without the LGTV before the 
#	command will fail. 
#	TestTv must equal "LGTV"
#	quary os the command, eg PowerOn

			TestTV = self.path.split('/') [-2]
			query = self.path.split('/') [-1]
						
			print (TestTV)
			print (query)

			
#			import pdb; pdb.set_trace()			

			if TestTV == "LGTV":
				success = LGTV_Commands.launch(query)
				print (success)
				if success == 'pass':
					self.end_headers()
					self.wfile.write(200); # OK
					self.wfile.write("   <POST OK>");
					print ("Post Completed Sucessfully...")
				else:
					self.end_headers()
					self.wfile.write(400);  # Bad Request
					self.wfile.write("   <POST FAIL>");
					print ("Post Failed...")           

		except:
			pass

def main():
    try:
        server = HTTPServer(('', 8000), MyHandler)
        print 'Started HTTP Server...'
        server.serve_forever()
    except KeyboardInterrupt:
        print '^C received, shutting down server'
        server.socket.close()

if __name__ == '__main__':
    main()
