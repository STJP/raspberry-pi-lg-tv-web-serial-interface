
# LG TV 50PS8 RS232 remote control 
# Adapted by Stephen Peek
# 
# Ver 1.0 

import re
import time
import LGTV_Serial

serial_port = "/dev/ttyAMA0"

def launch(query):
	cmd = (query)
	
	success = 'pass'
#	import pdb; pdb.set_trace()	

# Power On the TV
	if cmd == 'PowerOn':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_power(1)	
		lg._inst.close()

# Power Off the TV
	elif cmd == 'PowerOff':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_power(0)	
		lg._inst.close()

# Power On/Off the TV using Remote Command 08
	elif cmd == 'Power':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_remotepower()	
		lg._inst.close()

# Set inputs as required. 
	elif cmd == 'DTV':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('dtv')
		lg._inst.close()	

	elif cmd == 'HDMI-1':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('hdmi1')
		lg._inst.close()
		
	elif cmd == 'HDMI-2':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('hdmi2')
		lg._inst.close()
			
	elif cmd == 'HDMI-3':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('hdmi3')
		lg._inst.close()
	
	elif cmd == 'HDMI-4':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('hdmi4')
		lg._inst.close()
		
	elif cmd == 'VGA':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('vga')
		lg._inst.close()
		
	elif cmd == 'AV-1':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('av1')
		lg._inst.close()
				
	elif cmd == 'AV-2':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_input('av2')
		lg._inst.close()	

#	Mute On/Off
	elif cmd =='Mute':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_mute()
		lg._inst.close()

#	Info On/Off
	elif cmd =='Info':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_info()
		lg._inst.close()

#	Guide On/Off		
	elif cmd =='Guide':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_guide()
		lg._inst.close()
	
#	Set Volume to a specific Value
	elif cmd[:3] =='Vol':    # Isolates and checks the first 3 letters of the command for Vol
		VolLvl = int(cmd[-2:])	# extracts the Volume level, eg 10 and converts to an integer 
		print ("VL",VolLvl)
		lg = LGTV_Serial.LG(serial_port)
		lg.set_volume(VolLvl)
		lg._inst.close()

#	Set the Channel to a specific channel
	elif cmd[:2] =='Ch':    # Isolates and checks the first 2 letters of the command for Ch
		chan = int(cmd[-2:])	# extracts the Channel, eg 50  and converts to an integer
		print ("CH",chan)
		chanL = int(cmd[3:4])+10 # isolates and adds 10 to the first digit
		print ("CHL",chanL)
		chanR = int(cmd[-1:])+10 # isolates and adds 10 to the second digit
		print ("CHR",chanR)
		if chanL == 10:		# checks the first digit for "0" eg 05
			chanL = chanR	# then drops the 0 and sends the second digit + Ok
			chanR = 44		# Hex 44 = Ok
		chan = chanL
		count = 0	
		while (count < 2):
			lg = LGTV_Serial.LG(serial_port)
			lg.set_channel(chan)
			lg._inst.close()
			count = count + 1
			chan = chanR

	elif cmd =='UpArrow':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_uparrow()
		lg._inst.close()

	elif cmd =='DnArrow':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_dnarrow()
		lg._inst.close()		
	
	elif cmd =='RightArrow':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_rightarrow()
		lg._inst.close()	
			
	elif cmd =='LeftArrow':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_leftarrow()
		lg._inst.close()
		
	elif cmd =='OK':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_ok()
		lg._inst.close()

	elif cmd =='VUp':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_volup()
		lg._inst.close()

	elif cmd =='VDn':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_voldn()
		lg._inst.close()

	elif cmd =='ProgUp':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_progup()
		lg._inst.close()

	elif cmd =='ProgDn':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_progdn()
		lg._inst.close()

	elif cmd =='ReturnExit':
		lg = LGTV_Serial.LG(serial_port)
		lg.set_returnexit()
		lg._inst.close()
				
	else:
		success = 'fail'
	
	return success
	





