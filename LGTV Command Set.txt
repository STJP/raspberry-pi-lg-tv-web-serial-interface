
Command Structure 
Post -  http://AVSender:8000/LGTV/Command
Post -  http://[IP Address]:8000/LGTV/Command

Command 		LG Command

PowerOn			ka-1	
PowerOff		ka-0
DTV			xb-00
HDMI-1			xb-90
HDMI-2			xb-91
HDMI-3			xb-92
HDMI-4			xb-93
VGA			xb-60
AV-1			xb-20
AV-2			xb-21
Vol-[level]		kf-Level
			Level - 00 to 50
			eg Vol-10
 
Ch-[channel]		mc-Channel
			channel - 00 to 99
			eg Ch-05 (needs leading 0), Ch-22

Power			mc-08 
Mute			mc-09
Guide 			mc-AB
UpArrow			mc-40
DnArrow			mc-41
RightArrow		mc-06
LeftArrow		mc-07
OK			mc-44
Info			mc-AA
VUp			mc-02
VDn			mc-03
ProgUp			mc-00
ProgDn			mc-01
ReturnExit		mc-28

Auto start with crontab
sudo crontab -e
@reboot python /home/pi/LGTV/LGTV_Control_Webserver.py
