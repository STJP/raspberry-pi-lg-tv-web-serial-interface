# My project's README

This code was adapted from a couple of internet sources (apologies, I can't remember who) to control 
an LG 50PS8 series TV by the serial interface using a Raspberry Pi and RS232 GPIO Shield with commands sent via http post commands.

I put this project together to take the complexity out of controlling our AV system and used a ZIPATO Home Automation Controller to set up the various inputs to the LG TV and Yamaha RX-V675 amplifier.

Input commands are as simple as a Post Command to http://[IP Address]:8000/LGTV/Web Post Command

I set up auto start for the program using a Crontab input.

* [sudo crontab -e](Link URL)
* [@reboot python /home/pi/LGTV/LGTV_Control_Webserver.py](Link URL)